import argparse
import math
import os
import platform
import subprocess

import cv2
import numpy as np
import torch
from tqdm import tqdm

import audio
# from face_detect import face_rect
from models import Wav2Lip

from batch_face import RetinaFace
from time import time
import ffmpeg
from moviepy.editor import ImageSequenceClip, VideoFileClip
print(cv2.getBuildInformation())

parser = argparse.ArgumentParser(description='Inference code to lip-sync videos in the wild using Wav2Lip models')

var_checkpoint_path = 'checkpoints/wav2lip_gan.pth'

var_face = "sample_data/input_vid.mp4"

var_audio = "sample_data/input_audio.wav"

var_outfile = 'results/result_voice.mp4'

var_static = False
var_fps = 25

var_pads = [0, 10, 0, 0]

var_wav2lip_batch_size = 128

var_out_height = 480

var_crop= [0, -1, 0, -1]

var_box = [-1, -1, -1, -1]

var_rotate = False

var_nosmooth = True

var_img_size = 96

mel_step_size = 16
device = 'cuda' if torch.cuda.is_available() else 'cpu'
print('Using {} for inference.'.format(device))

full_frames = []
face_det_results = []
fps = 25

def main():
    s = time()

    # image_list = []
    size = 720*16//9, 720
    # frame_h, frame_w = __full_frames[0].shape[:-1]
    # out = cv2.VideoWriter('results/result_voice.mp4',
    #                         cv2.VideoWriter_fourcc(*'vp09'), fps, (frame_w, frame_h))
    # out = cv2.VideoWriter('results/result_voice.mp4',
    #                         cv2.VideoWriter_fourcc(*'avc1'), fps, (frame_w, frame_h))
    # out = cv2.VideoWriter('temp/result.avi',
    #                         cv2.VideoWriter_fourcc(*'DIVX'), fps, (frame_w, frame_h))
    out2 = cv2.VideoWriter('temp/result_3.mp4',
                            cv2.VideoWriter_fourcc(*'mp4v'), 25, (size[1], size[0]))
    xyz1 = time()
    for _ in range(25*4):
        data = np.random.randint(0, 256, size)
        # print(data[0], len(data[0]), size)
        out2.write(data)

    print("xyz1", time() - xyz1)
    # if device == 'cuda':
    #     torch.cuda.empty_cache()
    # out.release()
    out2.release()
    # return image_list
    # clip = ImageSequenceClip(image_list, fps=fps)
    # clip.write_videofile('results/result_voice123.mp4')
    # clip = VideoFileClip("temp/result.avi")
    # clip.write_videofile("results/myvideo.mp4")

    print("wav2lip prediction time:", time() - s)

    # endsss = time()

    # # Bỏ được đoạn này thì tốt
    # # in1 = ffmpeg.input("temp/result.avi")
    # # in2 = ffmpeg.input(var_audio)
    # # process = save_video(in2, in1, var_outfile)
    # # process.wait()

    # # clip = VideoFileClip("temp/result.avi")
    # # clip.write_videofile("results/myvideo.mp4")
    # subprocess.check_call([
    #     "ffmpeg", "-y",
    #     # "-vsync", "0", "-hwaccel", "cuda", "-hwaccel_output_format", "cuda",
    #     "-i", "temp/result.avi",
    #     "-i", var_audio,
    #     # "-c:v", "h264_nvenc",
    #     var_outfile,
    # ])
    # print("endsss", time() - endsss)

# if __name__ == '__main__':
#     args = parser.parse_args()
#     # do_load(var_checkpoint_path)
#     main()
