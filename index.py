# from flask import Flask, request, jsonify
# from flask_cors import CORS

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import requests
# from magic import Magic
import inference_custom
from time import time
import redis

import os
# from dotenv import load_dotenv

# load_dotenv()

PORT = os.getenv('PORT')
REDIS_IP = os.getenv('REDIS_IP')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD')
UPLOAD_URL = os.getenv('UPLOAD_URL')
print(PORT)

# app = Flask(__name__)
app = FastAPI()
# CORS(app)

origins = ["*"]

redis_connect = redis.Redis(host=REDIS_IP, port=REDIS_PORT, password=REDIS_PASSWORD, decode_responses=True)

app.add_middleware(
  CORSMiddleware,
  allow_origins=origins,
  allow_credentials=True,
  allow_methods=["*"],
  allow_headers=["*"],
)

class RequestBody(BaseModel):
  audio: str
  text: str

payload = {}
headers = {}

# mime = Magic(mime=True)
# inference.do_load('checkpoints/wav2lip_gan.pth')
@app.get("/")
def index():
   return { 
      "data": "OK"
   }

@app.post("/onnx")
async def home(data: RequestBody):
    step2 = time()
    inference_custom.main()
    print("Gen video: ", time() - step2)
    return {"data": "https://static.fqa.vn/fqa/sgk/result_voice_41cbf6b527.mp4"}


# if __name__ == "__main__":
#   from waitress import serve
# #   app.run(debug=True, host='0.0.0.0', port=8001)
#   serve(app, host="0.0.0.0", port=8001)